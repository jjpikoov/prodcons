#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <pthread.h>
#include <semaphore.h>

#define BUFFER_SIZE 10
#define PRODUCERS 5
#define CONSUMERS 5
#define ALL_THREADS PRODUCERS + CONSUMERS
#define RAND_DIVISOR 100000000
#define TRUE 1

void *producer(void *param);
void *consumer(void *param);
int insert_item(int item);
int remove_item(int* item);


pthread_mutex_t mutex;
sem_t full, empty;
int buffer[BUFFER_SIZE];
int buff_counter;


int main(void) {
    pthread_t threads_ids[ALL_THREADS];
    pthread_attr_t attr;
    int i = 0;
    
    pthread_mutex_init(&mutex, NULL);
    sem_init(&full, 0, 0);
    sem_init(&empty, 0, BUFFER_SIZE);
    pthread_attr_init(&attr);
    buff_counter = 0;
    
    for(i = 0; i < PRODUCERS; i++) {
        pthread_create(&threads_ids[i], &attr, producer, NULL);
    }
    
    for(; i < ALL_THREADS; i++) {
        pthread_create(&threads_ids[i], &attr, consumer, NULL);
    }
    
    pthread_exit(NULL);
    exit(0);
}

void *producer(void *param) {
    
    while(TRUE) {
        
        int r_num = rand() / RAND_DIVISOR;
        sleep(r_num);
        int item = rand();
        
        sem_wait(&empty);
        pthread_mutex_lock(&mutex);
        
        if(insert_item(item)) {
            fprintf(stderr, "Producer error\n");
        }
        else {
            printf("Producer created %d\n", item);
        }
        
        pthread_mutex_unlock(&mutex);
        sem_post(&full);
    }
}

void *consumer(void *param) {
    int item;
    
    while(TRUE) {
        int r_num = rand() / RAND_DIVISOR;
        sleep(r_num);
        
        sem_wait(&full);
        pthread_mutex_lock(&mutex);
        
        if(remove_item(&item)) {
            fprintf(stderr, "Consumer error\n");
        }
        else {
            printf("Consumer consumed %d\n", item);
        }
        
        pthread_mutex_unlock(&mutex);
        sem_post(&empty);
    }
}

int insert_item(int item) {
    if(buff_counter < BUFFER_SIZE) {
        buffer[buff_counter] = item;
        buff_counter++;
        return 0;
    }
    else { 
        return -1;
    }
}

int remove_item(int* item) {
    if(buff_counter > 0) {
        buff_counter--;
        *item = buffer[buff_counter];
        return 0;
    }
    else {
        return -1;
    }
}
